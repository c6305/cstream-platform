#!/usr/bin/env bash

# Add more custom commands here if needed

set -e
forever start /var/www/html/public/nodejs/main.js
exec supervisord -c /etc/supervisor/supervisord.conf
